.PHONY: build
build:
	docker-compose build

.PHONY: run
run:
	docker-compose run

.PHONY: prod
prod:
	docker-compose -f docker-compose.dev.yml up --build --detach


.PHONY: prod-build
build-build:
	docker-compose -f docker-compose.dev.yml build