import '@fontsource/roboto';
import Login from './views/Login';
import CitiesList from './views/city/CitiesList';
import Room from './views/room/Room';
import Office from './views/office/OfficeList';
import React, { Fragment } from 'react';
import Home from './views/Home';
import Error from './views/error/error';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { history } from './helpers/history';
import { PrivateRoute } from './components/PrivateRoute';
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

if (process.env.NODE_ENV === 'production') {
  var console = {};
  console.log = function () {};
}

const theme = createTheme({
  palette: {
    secondary: {
      main: '#df2745',
    },
    primary: {
      main: '#171f46',
    },
  },
});

const App = ({ props }) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Router history={history}>
        <Switch>
          <Route
            exact
            path="/login"
            component={(routeProps) => <Login {...routeProps} />}
          />

          <PrivateRoute
            exact
            path="/cities"
            component={(routeProps) => <CitiesList {...routeProps} />}
          />
          <PrivateRoute
            exact
            path="/office/:id"
            component={(routeProps) => <Office {...routeProps} />}
          />
          <PrivateRoute
            exact
            path="/room/:id"
            component={(routeProps) => <Room {...routeProps} />}
          />
          <PrivateRoute
            exact
            path="/"
            component={(routeProps) => <Home {...routeProps} />}
          />
          <Route path="/error" component={Error} />
          <Redirect to="/error" />
        </Switch>
      </Router>
    </MuiThemeProvider>
  );
};

export default App;
