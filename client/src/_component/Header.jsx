import React, { useEffect, useMemo, useState } from 'react';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Avatar from '@material-ui/core/Avatar';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import HomeIcon from '@material-ui/icons/Home';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { userActions } from '../redux/actions/user';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      minHeight: '95px',
      alignItems: 'flex-start',
    },
  },
  title: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',

    [theme.breakpoints.down('xs')]: {
      bottom: '100%',
    },
  },
  flex: {
    display: 'flex',
    alignItems: 'center',
  },
  link: {
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('xs')]: {
      marginTop: '17px',
    },
  },
  icon: {
    marginRight: 15,
    '& path': {
      fill: 'white',
      stroke: 'white',
    },
  },
  invisible: {
    minWidth: 63,
  },
  avatar: {
    width: "44px",
    height: "44px",
  }
}));

const Header = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [visible, setVisible] = useState(true);
  const open = Boolean(anchorEl);
  const dispatch = useDispatch();
  const userType = useSelector((state) =>  state.userInfoReducer.user);
  const history = useHistory();
  const localData = JSON.parse(localStorage.getItem('user'))

  useEffect(() => {
    !userType?.length && dispatch(userActions.getUserInfo(localData.user_id));
  }, []);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  useEffect(() => {
    history.location.pathname !== '/login' &&
    history.location.pathname !== '/error'
      ? setVisible(true)
      : setVisible(false);
  });

  const goBack = () => {
    history.goBack();
  };

  return (
    <React.Fragment>
      {visible && (
        <AppBar id="appbar" position="fixed" color="secondary">
          <Toolbar className={classes.header}>
            {history.location.pathname !== '/' ? (
              <div className={classes.link}>
                <Link to="/">
                  <HomeIcon
                    style={{ color: '#fff', cursor: 'pointer' }}
                    className={classes.icon}
                  />
                </Link>

                {history.location.pathname !== '/' && (
                  <ArrowBackIcon
                    onClick={goBack}
                    style={{ color: '#fff', cursor: 'pointer' }}
                  />
                )}
              </div>
            ) : (
              <div className={classes.invisible}> </div>
            )}

            <Typography variant="h6" className={classes.title}>
              Office Mapper
            </Typography>
            <div className={classes.flex}>
              <Typography className={classes.user_text} component="p">
                {userType?.first_name} {userType?.last_name}
              </Typography>
              <div>
                <IconButton
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleMenu}
                  color="inherit"
                >
                  <Avatar className={classes.avatar}
                    alt={userType?.first_name + ' ' + userType?.last_name}
                    src={userType?.image_url}
                  />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={handleClose}
                >
                  <MenuItem onClick={handleClose} component={Link} to={'/'}>
                    Profile
                  </MenuItem>
                  <MenuItem
                    onClick={handleClose}
                    component={Link}
                    to={'/login'}
                  >
                    Logout
                  </MenuItem>
                </Menu>
              </div>
            </div>
          </Toolbar>
        </AppBar>
      )}
      <Toolbar className={classes.header} />
    </React.Fragment>
  );
};
export default Header;
