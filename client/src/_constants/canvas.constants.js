// adjusting cursor view depending on position
const cursorForPosition = (position) => {
  switch (position) {
    case 'tl':
    case 'br':
    case 'start':
    case 'end':
      return 'nwse-resize';
    case 'tr':
    case 'bl':
      return 'nesw-resize';
    default:
      return 'move';
  }
};
const convertPoints = (x1, y1, x2, y2, type) => {
  const mainBlock = document.getElementById('container');

    const weidth = mainBlock.offsetWidth;
    const height = mainBlock.offsetHeight;
  if (type === 'toPixels') {
    const convX1 = x1 * weidth
    const convY1 = y1 * height
    const convX2 = x2 * weidth
    const convY2 = y2 * height
    return [convX1, convY1, convX2, convY2]
  } else if (type === 'toPercent') {
    const convX1 = x1 / weidth
    const convY1 = y1 / height
    const convX2 = x2 / weidth
    const convY2 = y2 / height
    return [convX1, convY1, convX2, convY2]
  }
}
// calculating table corners and space inside
const positionWithinTable = (x, y, table) => {
  const [x1, y1, x2, y2] = convertPoints(table.x1, table.y1, table.x2, table.y2, 'toPixels') 
  const topLeft = nearPoint(x, y, x1, y1, 'tl');
  const topRight = nearPoint(x, y, x2, y1, 'tr');
  const bottomLeft = nearPoint(x, y, x1, y2, 'bl');
  const bottomRight = nearPoint(x, y, x2, y2, 'br');
  const inside = x >= x1 && x <= x2 && y >= y1 && y <= y2 ? 'inside' : null;
  return topLeft || topRight || bottomLeft || bottomRight || inside;
};

// calculating size of table corners
const nearPoint = (x, y, x1, y1, name) => {
  return Math.abs(x - x1) < 10 && Math.abs(y - y1) < 10 ? name : null;
};

// getting table at specified x and y
const getTableAtPosition = (x, y, tables) => {
  return tables
    .map((table) => ({ ...table, position: positionWithinTable(x, y, table) }))
    .find((table) => table.position !== null);
};

// resizing table coordinates depending on mouse position
const resizedCoordinates = (clientX, clientY, position, coordinates) => {
  const [x1, y1, x2, y2] = convertPoints(coordinates.x1, coordinates.y1, coordinates.x2, coordinates.y2, 'toPixels') 
  switch (position) {
    case 'tl':
    case 'start':
      return { x1: clientX, y1: clientY, x2, y2 };
    case 'tr':
      return { x1, y1: clientY, x2: clientX, y2 };
    case 'bl':
      return { x1: clientX, y1, x2, y2: clientY };
    case 'br':
    case 'end':
      return { x1, y1, x2: clientX, y2: clientY };
    default:
      return null; //should not really get here...
  }
};

export const CANVAS_CONSTANTS={
  cursorForPosition,
  getTableAtPosition,
  resizedCoordinates,
  convertPoints
}