import { makeStyles, createTheme } from '@material-ui/core/styles';
const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
});
const STYLES_CITY = makeStyles((theme) => ({
  root: {
    width: 320,
    height: 260,
    textAlign: 'center',
    margin: theme.spacing(1),
    padding: theme.spacing(2),
    background: '#f9f9f9',
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
  },
  country: {
    fontWeight: '700',
    fontSize: 30,
  },
  city: {
    fontSize: 24,
    marginBottom: 10,
  },
  name: {
    fontSize: 16,
    marginBottom: 10,
  },
  address: {
    marginTop: "1rem",
    '& > p': {
      fontWeight: 700
    }   
  },
  actionCityList: {
    margin: 'auto',
    position: 'absolute',
    bottom: 10,
    justifyContent: 'center',
    alignSelf: 'center',
  },
}));

const STYLES_HEADER = makeStyles({
  root: {
    position: 'fixed',
    top: 0,
    width: '100%',
    height: 60,
    display: 'flex',
    background: '#f9f9f9',
    boxShadow: '0 3px 0 0 hsl(0deg 0% 76% / 64%)',
    padding: 10,
    margin: 0,
  },
  content: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0 20px',
    '&:last-child': {
      paddingBottom: 0,
    },
  },
  title: {
    fontWeight: 600,
    fontSize: 35,
  },
  user_block: {
    display: 'flex',
    marginRight: 40,
    fontSize: 20,
  },
  user_text: {
    fontSize: 28,
    marginRight: 20,
  },
  logout: {
    fontSize: 20,
    textTransform: 'none',
  },
  [theme.breakpoints.down('sm')]: {
    title: {
      fontWeight: 600,
      fontSize: 21,
    },
    user_text: {
      fontSize: 18,
      marginRight: 10,
    },
    logout: {
      fontSize: 15,
    },
  },
});
const STYLES_FLOOR = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: 100,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'auto',
    height: 'calc(100vh - 250px)',
    padding: '0',
    margin: 0,
  },
  item: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    background: '#f9f9f9',
    boxShadow: '0 3px 0 0 hsl(0deg 0% 76% / 64%)',
    marginBottom: '15px',
    padding:'25px !important',
    '&:last-child': {
      marginBottom: 0,
    },
  },
  item_text: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
  },
  accordion: {
    padding: theme.spacing(2),
    background: '#f9f9f9',
  },
  card: {
    fontWeight: "bold",
    padding: theme.spacing(4),
    margin: theme.spacing(1),
    // color: theme.palette.secondary.main,
    cursor: 'pointer',
    textDecoration: 'none',
    '&:hover': {
      background: '#f9f9f9',
    }
  },
}));

export const STYLES_CONSTANTS = {
  STYLES_HEADER,
  STYLES_CITY,
  STYLES_FLOOR,
};


