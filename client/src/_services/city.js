import { DOMEN, LOCALHOST_DOMEN } from '../_constants/other';
import axiosApiInstance from '../helpers/axiosInstance';
import 'regenerator-runtime/runtime';
export const cityService = {
  getAllCities,
};

async function getAllCities() {
  const result = await axiosApiInstance.get(
    `${DOMEN || LOCALHOST_DOMEN}/api/v1/offices/`
  );
  return result;
}
