import { DOMEN, LOCALHOST_DOMEN } from '../_constants/other';
import axiosApiInstance from '../helpers/axiosInstance';
import 'regenerator-runtime/runtime';
export const officeService = {
  getOffice,
};

async function getOffice(id) {
  const result = await axiosApiInstance.get(
    `${DOMEN || LOCALHOST_DOMEN}/api/v1/offices/${id}/`
  );
  return result;
}
