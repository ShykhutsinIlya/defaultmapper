import { DOMEN, LOCALHOST_DOMEN } from '../_constants/other';
import axiosApiInstance from '../helpers/axiosInstance';
import 'regenerator-runtime/runtime';
export const roomService = {
  getRoom,
};

async function getRoom(id) {
  const result = await axiosApiInstance.get(
    `${DOMEN || LOCALHOST_DOMEN}/api/v1/rooms/${id}/`
  );
  return result;
}
