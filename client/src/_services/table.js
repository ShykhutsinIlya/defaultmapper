import { DOMEN, LOCALHOST_DOMEN } from '../_constants/other';
import axiosApiInstance from '../helpers/axiosInstance';
import 'regenerator-runtime/runtime';
export const tableService = {
  postTable,
  patchTable,
  deleteTable,
};

async function postTable(table) {
  const requestOptions = {
    body: table,
  };
  const result = await axiosApiInstance.post(
    `${DOMEN || LOCALHOST_DOMEN}/api/v1/tables/`,
    requestOptions
  );
  return result;
}
async function patchTable(table, id) {
  const requestOptions = {
    body: table,
  };
  const result = await axiosApiInstance.patch(
    `${DOMEN || LOCALHOST_DOMEN}/api/v1/tables/${id}/`,
    requestOptions
  );
  return result;
}
async function deleteTable(id) {
  const result = await axiosApiInstance.delete(
    `${DOMEN || LOCALHOST_DOMEN}/api/v1/tables/${id}/`
  );

  return result;
}
