import { DOMEN, LOCALHOST_DOMEN } from '../_constants/other';
import 'regenerator-runtime/runtime';
import axiosApiInstance from '../helpers/axiosInstance';
import axios from 'axios';
export const userService = {
  login,
  logout,
  getAllUsers,
  getUserInfo,
};

function login(token) {
  const requestOptions = {
    headers: { 'Content-Type': 'application/json' },
    grant_type: 'convert_token',
    token: token,
    backend: 'google-oauth2',
    client_id: process.env.REACT_APP_DJANGO_CLIENT_ID,
  };

  return axios
    .post(
      `${DOMEN || LOCALHOST_DOMEN}/api/v1/users/auth/convert-token/`,
      requestOptions
    )
    .then((user) => {
      let data = { ...user.data, image: localStorage.getItem('user') };
      data && localStorage.setItem('user', JSON.stringify(data));
      return data;
    });
}

function logout() {
  localStorage.removeItem('user');
}

async function getAllUsers() {
  const result = await axiosApiInstance.get(
    `${DOMEN || LOCALHOST_DOMEN}/api/v1/users/users/`
  );
  return result;
}

async function getUserInfo(id) {
  const result = await axiosApiInstance.get(
    `${DOMEN || LOCALHOST_DOMEN}/api/v1/users/users/${id}/`
  );

  return result;
}
