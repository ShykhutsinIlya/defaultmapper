export function authHeader() {
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.access_token) {
        return 'Bearer ' + user.access_token;
    } else {
        return '';
    }
}

export function refreshHeader() {
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.refresh_token) {
        return user.refresh_token;
    } else {
        return '';
    }
}