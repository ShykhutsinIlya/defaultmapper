import { cityConstants } from '../constant/city';
import { cityService } from '../../_services/city';

export const cityActions = {
  getAllCities,
  clearCityError
};

function clearCityError() {
  return { type: cityConstants.CLEAR_ERROR };
}

function getAllCities() {
  return (dispatch) => {
    dispatch(request());

    cityService.getAllCities().then(
      (cities) => {
        dispatch(success(cities));
      },
      (error) => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: cityConstants.CITY_LIST_REQUEST };
  }
  function success(cities) {
    return { type: cityConstants.CITY_LIST_SUCCESS, cities };
  }
  function failure(error) {
    return { type: cityConstants.CITY_LIST_FAILURE, error };
  }
}
