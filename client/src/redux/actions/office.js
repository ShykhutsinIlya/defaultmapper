import { officeConstants } from '../constant/office';
import { officeService } from '../../_services/office';

export const officeActions = {
  getOffice,
  clearOfficeError
};
function clearOfficeError() {
  return { type: officeConstants.CLEAR_ERROR };
}
function getOffice(id) {
  return (dispatch) => {
    dispatch(request());

    officeService.getOffice(id).then(
     
      (office) => {
        dispatch(success(office));
      },
      (error) => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: officeConstants.OFFICE_REQUEST };
  }
  function success(office) {
    return { type: officeConstants.OFFICE_SUCCESS, office };
  }
  function failure(error) {
    return { type: officeConstants.OFFICE_FAILURE, error };
  }
}
