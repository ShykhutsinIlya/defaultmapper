import { roomConstants } from '../constant/room';
import { roomService } from '../../_services/room';

export const roomActions = {
  getRoom,
  clearRoomError
};
function clearRoomError() {
  return { type: roomConstants.CLEAR_ERROR };
}
function getRoom(id) {
  return (dispatch) => {
    dispatch(request());

    roomService.getRoom(id).then(
      (room) => {
        dispatch(success(room));
      },
      (error) => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: roomConstants.ROOM_REQUEST };
  }
  function success(room) {
    return { type: roomConstants.ROOM_SUCCESS, room };
  }
  function failure(error) {
    return { type: roomConstants.ROOM_FAILURE, error };
  }
}
