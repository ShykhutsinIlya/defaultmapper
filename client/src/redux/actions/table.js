import { tableConstants } from '../constant/table';
import { tableService } from '../../_services/table';

export const tableActions = {
  postTable,
  patchTable,
  deleteTable,
};

function postTable(table) {
  return (dispatch) => {
    dispatch(request());

    tableService.postTable(table);
  };

  function request() {
    return { type: tableConstants.TABLE_POST_REQUEST };
  }
  function success(table) {
    return { type: tableConstants.TABLE_POST__SUCCESS, table };
  }
  function failure(error) {
    return { type: tableConstants.TABLE_POST__FAILURE, error };
  }
}
function patchTable(table, id) {
  return (dispatch) => {
    dispatch(request());

    tableService.patchTable(table, id);
  };

  function request() {
    return { type: tableConstants.TABLE_PATCH_REQUEST };
  }
  function success(table) {
    return { type: tableConstants.TABLE_PATCH_SUCCESS, table };
  }
  function failure(error) {
    return { type: tableConstants.TABLE_PATCH_FAILURE, error };
  }
}

function deleteTable(id) {
  return (dispatch) => {
    dispatch(request());

    tableService.deleteTable(id);
  };

  function request() {
    return { type: tableConstants.TABLE_DELETE_REQUEST };
  }
  function success(table) {
    return { type: tableConstants.TABLE_DELETE_SUCCESS, table };
  }
  function failure(error) {
    return { type: tableConstants.TABLE_DELETE_FAILURE, error };
  }
}
