import { userConstants } from '../constant/user';
import { userService } from '../../_services/user';

export const userActions = {
  login,
  logout,
  getAllUsers,
  getUserInfo
};
function getAllUsers() {
  return (dispatch) => {
    dispatch(request());

    userService.getAllUsers().then(
      (users) => {
        dispatch(success(users));
      },
      (error) => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: userConstants.USER_REQUEST };
  }
  function success(users) {
    return { type: userConstants.USER_SUCCESS, users };
  }
  function failure(error) {
    return { type: userConstants.USER_FAILURE, error };
  }
}

function login(token) {
  return (dispatch) => {
    dispatch(request({ token }));

    userService.login(token).then(
      (user) => {
        dispatch(success(user));
      },
      (error) => {
        dispatch(failure(error));
      }
    );
  };

  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
}

function getUserInfo(id) {
  return (dispatch) => {
    dispatch(request({ id }));

    userService.getUserInfo(id).then(
      (user) => {
        dispatch(success(user));
      },
      (error) => {
        dispatch(failure(error));
      }
    );
  };

  function request(user) {
    return { type: userConstants.USER_INFO_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.USER_INFO_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.USER_INFO_FAILURE, error };
  }
}

function logout() {
  userService.logout();
  return { type: userConstants.LOGOUT };
}
