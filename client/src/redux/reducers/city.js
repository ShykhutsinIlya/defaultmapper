import { cityConstants } from '../constant/city';

const initialState = { loader: false, cities: {}, error: '' };

export function citiesReducer(state = initialState, action) {
  switch (action.type) {
    case cityConstants.CITY_LIST_REQUEST:
      return {
        ...state,
        error: '',
        loader: true,
      };
    case cityConstants.CITY_LIST_SUCCESS:
      return {
        ...state,
        error: '',
        loader: false,
        cities: action.cities,
      };
    case cityConstants.CITY_LIST_FAILURE:
      return {
        ...state,
        loader: false,
        error: action.error,
      };
    case cityConstants.CLEAR_ERROR:
      return {
        ...state,
        error: "",
      };
    default:
      return state;
  }
}
