import { officeConstants } from '../constant/office';

const initialState = { loader: false, office: null, error: '' };

export function officeReducer(state = initialState, action) {
  switch (action.type) {
    case officeConstants.OFFICE_REQUEST:
      return {
        ...state,
        error: '',
        loader: true,
      };
    case officeConstants.OFFICE_SUCCESS:
      return {
        ...state,
        error: '',
        loader: false,
        office: action.office,
      };
    case officeConstants.OFFICE_FAILURE:
      return {
        ...state,
        loader: false,
        error: action.error,
      };
    case officeConstants.CLEAR_ERROR:
      return {
        ...state,
        error: '',
      };
    default:
      return state;
  }
}
