import { roomConstants } from '../constant/room';

const initialState = { loader: false, room: null, error: '' };

export function roomReducer(state = initialState, action) {
  switch (action.type) {
    case roomConstants.ROOM_REQUEST:
      return {
        ...state,
        error: '',
        loader: true,
      };
    case roomConstants.ROOM_SUCCESS:
      return {
        ...state,
        error: '',
        loader: false,
        room: action.room,
      };
    case roomConstants.ROOM_FAILURE:
      return {
        ...state,
        loader: false,
        error: action.error,
      };
    case roomConstants.CLEAR_ERROR:
      return {
        ...state,
        error: '',
      };
    default:
      return state;
  }
}
