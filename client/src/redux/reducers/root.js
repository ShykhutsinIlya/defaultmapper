import { combineReducers } from 'redux';

import { userReducer, userInfoReducer } from '../reducers/user';
import { authentication } from '../reducers/authentication';
import { citiesReducer } from '../reducers/city';
import { officeReducer } from '../reducers/office';
import { roomReducer } from '../reducers/room';
import { tableReducer }from '../reducers/table';
const rootReducer = combineReducers({
  authentication,
  citiesReducer,
  officeReducer,
  roomReducer,
  userReducer,
  tableReducer,
  userInfoReducer
});

export default rootReducer;
