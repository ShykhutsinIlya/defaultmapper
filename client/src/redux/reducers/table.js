import { tableConstants } from '../constant/table';

const initialState = { loader: false, tables: null, error: '' };

export function tableReducer(state = initialState, action) {
  switch (action.type) {
    case tableConstants.TABLE_PATCH_REQUEST:
      return {
        ...state,
      };
    case tableConstants.TABLE_PATCH_SUCCESS:
      return {
        ...state,
      };
    case tableConstants.TABLE_PATCH_FAILURE:
      return {
        ...state,
      };
    case tableConstants.TABLE_POST_REQUEST:
      return {
        ...state,
      };
    case tableConstants.TABLE_POST__SUCCESS:
      return {
        ...state,
      };
    case tableConstants.TABLE_POST__FAILURE:
      return {
        ...state,
      };
    case tableConstants.TABLE_DELETE_REQUEST:
      return {
        ...state,
      };
    case tableConstants.TABLE_DELETE_SUCCESS:
      return {
        ...state,
      };
    case tableConstants.TABLE_DELETE_FAILURE:
      return {
        ...state,
      };
    default:
      return state;
  }
}
