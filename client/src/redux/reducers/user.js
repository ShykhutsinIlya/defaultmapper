import { userConstants } from '../constant/user';

const initialState = { loader: false, users: [], error: '' };

export function userReducer(state = initialState, action) {
  switch (action.type) {
    case userConstants.USER_REQUEST:
      return {
        ...state,
        error: '',
        loader: true,
      };
    case userConstants.USER_SUCCESS:
      return {
        ...state,
        loader: false,
        error: '',
        users: action.users,
      };
    case userConstants.USER_FAILURE:
      return {
        ...state,
        loader: false,
        error: action.error,
      };
    default:
      return state;
  }
}

const initialInfoState = { loader: false, user: {}, error: '' };

export function userInfoReducer(state = initialInfoState, action) {
  switch (action.type) {
    case userConstants.USER_INFO_REQUEST:
      return {
        ...state,
        error: '',
        loader: true,
      };
    case userConstants.USER_INFO_SUCCESS:
      return {
        ...state,
        loader: false,
        error: '',
        user: action.user,
      };
    case userConstants.USER_INFO_FAILURE:
      return {
        ...state,
        loader: false,
        error: action.error,
      };
    default:
      return state;
  }
}
