import React, { useEffect }from 'react';
import { connect } from 'react-redux';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import EmailIcon from '@material-ui/icons/Email';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';
import Fab from '@material-ui/core/Fab';
import BusinessIcon from '@material-ui/icons/Business';
import Tooltip from '@material-ui/core/Tooltip';
import { Link } from 'react-router-dom';
import GoogleMapReact from 'google-map-react';
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { userActions } from '../redux/actions/user';
import CircularProgress from '@material-ui/core/CircularProgress';
import Header from '../_component/Header';


const useStyles = makeStyles((theme) => ({

    paper: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        padding: theme.spacing(2),
        backgroundColor: "#f9f9f9",
        position: "relative"
    },
    avatar: {
        width: theme.spacing(20),
        height: theme.spacing(20),
    },
    avatarIcon: {
        background: theme.palette.secondary.main,
    },
    fab: {
        position: 'absolute',
        top: theme.spacing(2),
        right: theme.spacing(2),
    }
  }));


const Home = (props) => {
    const localData = JSON.parse(localStorage.getItem('user'))
    let id = localData.user_id
    const dispatch = useDispatch();
    const user = useSelector((state) => state.userInfoReducer.user);
    
    useEffect(() => {
        !user?.length && dispatch(userActions.getUserInfo(id));
    }, []);

    const classes = useStyles();
    const location = user?.city && user?.address ? `${user.city}, ${user.address}` : "Not assigned"
    const room = user?.room_number ? user.room_number : "Not assigned"
    return (
        <>
      <Header />
      <Container>
            { user ? <>
            <Paper className={classes.paper}>
                <Grid
                    container
                    direction="column"
                    justifyContent="space-around"
                    alignItems="center"
                    spacing={3}
                > 
                    <Grid item>
                        <Avatar className={classes.avatar} alt={user.first_name + " " + user.last_name} src={user.image_url} />
                    </Grid>
                    <Grid item style={{width: "100%"}}>
                        <Grid
                            container
                            direction="row"
                            justifyContent="flex-start"
                            alignItems="center"
                        > 
                            <Grid item xs={12} sm={6} md={4}>
                                <List>
                                    <ListItem>
                                        <ListItemAvatar>
                                        <Avatar>
                                            <AccountCircleIcon/>
                                        </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText 
                                            primaryTypographyProps={{color: "secondary", variant: "button"}} 
                                            secondaryTypographyProps={{color: "primary", variant: "subtitle2"}} 
                                            primary="First Name" 
                                            secondary={user.first_name}
                                        />
                                    </ListItem>
                                    <ListItem>
                                        <ListItemAvatar>
                                        <Avatar>
                                            <SupervisedUserCircleIcon />
                                        </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText 
                                            primaryTypographyProps={{color: "secondary", variant: "button"}} 
                                            secondaryTypographyProps={{color: "primary", variant: "subtitle2"}}
                                            primary="Last Name" 
                                            secondary={user.last_name} 
                                        />
                                    </ListItem>
                                </List>
                            </Grid>
                            <Grid item xs={12} sm={6} md={4}>
                                <List>
                                    <ListItem>
                                        <ListItemAvatar>
                                        <Avatar>
                                            <LocationOnIcon />
                                        </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText 
                                            primaryTypographyProps={{color: "secondary", variant: "button"}} 
                                            secondaryTypographyProps={{color: "primary", variant: "subtitle2"}}
                                            primary="Location" 
                                            secondary={location} // change to real address when ready on back
                                        />
                                    </ListItem>
                                    <ListItem>
                                        <ListItemAvatar>
                                        <Avatar>
                                            <PersonPinCircleIcon />
                                        </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText 
                                            primaryTypographyProps={{color: "secondary", variant: "button"}} 
                                            secondaryTypographyProps={{color: "primary", variant: "subtitle2"}}
                                            primary="Room" 
                                            secondary={room} // change to real room when ready on back
                                        />
                                    </ListItem>
                                </List>
                            </Grid>
                            <Grid item xs={12} sm={6} md={4}>
                                <List>
                                    <ListItem>
                                        <ListItemAvatar>
                                        <Avatar>
                                            <EmailIcon />
                                        </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText 
                                            primaryTypographyProps={{color: "secondary", variant: "button"}} 
                                            secondaryTypographyProps={{color: "primary", variant: "subtitle2"}}
                                            primary="Email" 
                                            secondary={user.email} 
                                        />
                                    </ListItem>
                                    <ListItem>
                                        <ListItemAvatar>
                                        <Avatar>
                                            <PersonAddIcon />
                                        </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText 
                                            primaryTypographyProps={{color: "secondary", variant: "button"}} 
                                            secondaryTypographyProps={{color: "primary", variant: "subtitle2"}}
                                            primary="Admin" 
                                            secondary={user.is_staff ? "Yes" : "No"} 
                                        />
                                    </ListItem>
                                </List>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Tooltip arrow component={Link} to={'/cities'} title="Manage Employees Placement">
                    <Fab className={classes.fab} color="secondary" aria-label="add">
                        <BusinessIcon />
                    </Fab>
                </Tooltip>
            </Paper>
            <Paper>
                <div style={{ height: '45vh', width: '100%', marginBottom: '1rem'}}>
                    <GoogleMapReact
                    bootstrapURLKeys={{ key: process.env.REACT_APP_GOOPLE_MAP_KEY }}
                    defaultCenter={{lat: 53.893009, lng: 27.567444}}
                    defaultZoom={10}
                    >   
                        {user.coordinates && <PersonPinCircleIcon fontSize="large" color="secondary" // change to real person coordinates when ready on back
                            lat={user.coordinates?.latitude}
                            lng={user.coordinates?.longitude}
                        /> }
                    </GoogleMapReact>
                </div>
            </Paper>
            </> : <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '90vh'}}>
                <CircularProgress size={70} color="secondary"/>
            </div> }
        </Container>
      </>
       
    )
}

  export default Home