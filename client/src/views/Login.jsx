import React, { useState } from 'react';
import logo from '../logo.png';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import { GoogleLogin } from 'react-google-login';
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux';
import { userActions } from '../redux/actions/user';
import { Link } from 'react-router-dom';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import Header from '../_component/Header';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(0),
    height: '100vh',
  },
  header: {
    marginTop: theme.spacing(3),
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    textAlign: 'center',
  },
  description: {
    color: '#9e9e9e',
  },
  button: {
    marginTop: theme.spacing(5),
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    minWidth: '120px',
  },
  imgContainer: {
    height: '100vh',
  },
  image: {
    objectFit: 'cover',
    height: '100%',
    width: '100%',
  },
}));

const Login = (props) => {
  const classes = useStyles();
  const onSuccessGoogle = (response) => {
    handleLogin(response.accessToken);
  };

  const onFailureGoogle = (response) => {
    console.log(response);
  };

  const handleLogin = (token) => {
    const { dispatch } = props;
    if (token) {
      dispatch(userActions.login(token));
    }
  };

  const handleLogout = () => {
    const { dispatch } = props;
    dispatch(userActions.logout());
  };

  const { loggingIn, loggedIn, user } = props;
  return (
    <>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        className={classes.root}
      >
        <Grid item lg={6} md={6}>
          <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="center"
          >
            <img alt="INNOWISE" src={logo} width="30%" />
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
              className={classes.header}
            >
              <Typography variant="button" component="span" color="secondary">
                Office Mapper
              </Typography>
              <Typography
                className={classes.description}
                variant="subtitle1"
                component="span"
              >
                Employees office placement management tool
              </Typography>
            </Grid>
            {loggedIn ? (
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
                className={classes.header}
              >
                <Typography
                  variant="subtitle1"
                  component="span"
                  color="primary"
                >
                  You are logged in as{' '}
                  <b>
                    {user.first_name} {user.last_name}{' '}
                  </b>
                </Typography>
                <Grid
                  container
                  direction="row"
                  justifyContent="center"
                  alignItems="center"
                  className={classes.header}
                >
                  <Button
                    component={Link}
                    to={'/'}
                    variant="contained"
                    color="secondary"
                    className={classes.button}
                    endIcon={<ExitToAppIcon />}
                  >
                    To App
                  </Button>
                  <Button
                    onClick={handleLogout}
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    endIcon={<RemoveCircleOutlineIcon />}
                  >
                    Logout
                  </Button>
                </Grid>
              </Grid>
            ) : !loggingIn ? (
              <GoogleLogin
                clientId={process.env.REACT_APP_OAUTH_CLIENT_ID}
                render={(renderProps) => (
                  <Button
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                    variant="contained"
                    color="secondary"
                    className={classes.button}
                    endIcon={<PersonOutlineIcon />}
                  >
                    Log In
                  </Button>
                )}
                buttonText="Login"
                onSuccess={onSuccessGoogle}
                onFailure={onFailureGoogle}
                cookiePolicy={'single_host_origin'}
              />
            ) : (
              <CircularProgress color="secondary" />
            )}
          </Grid>
        </Grid>
        <Hidden only={['xs', 'sm']}>
          <Grid item lg={6} md={6}>
            <div className={classes.imgContainer}>
              <img
                alt=""
                className={classes.image}
                src="https://pcsniagara.com/wp-content/uploads/2016/08/clean-office.jpg"
              />
            </div>
          </Grid>
        </Hidden>
      </Grid>
    </>
  );
};

function mapStateToProps(state) {
  const { loggingIn, loggedIn, user } = state.authentication;
  return {
    loggingIn,
    loggedIn,
    user,
  };
}

export default connect(mapStateToProps)(Login);
