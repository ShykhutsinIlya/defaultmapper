import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CityItem from './CityItem';
import './css/index.css';
import { cityActions } from '../../redux/actions/city';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import DashboardIcon from '@material-ui/icons/Dashboard';
import MapIcon from '@material-ui/icons/Map';
import { Link, Redirect } from 'react-router-dom';
import GoogleMapReact from 'google-map-react';
import Paper from '@material-ui/core/Paper';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import Tooltip from '@material-ui/core/Tooltip';
import CircularProgress from '@material-ui/core/CircularProgress';
import Header from '../../_component/Header';

const CitiesList = () => {
  const dispatch = useDispatch();
  const cities = useSelector((state) => state.citiesReducer.cities);
  const loader = useSelector((state) => state.citiesReducer.loader);
  // const citiesList = useMemo(
  //   () => cities?.length && cities.filter((city) => city.floor_count),
  //   [cities]
  // );
  const errorCity = useSelector((state) => state.citiesReducer.error);

  useEffect(() => {
    !cities?.length && dispatch(cityActions.getAllCities());
  }, []);

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        // style={{ display: citiesList?.length ? 'block' : 'none' }}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <div>{children}</div>}
      </div>
    );
  }

  return (
    <>
      {errorCity && <Redirect to="/error" />}
      <Header />
      <Container>
        {!loader ? (
          <>
            <Tabs
              value={value}
              onChange={handleChange}
              centered
              indicatorColor="secondary"
              textColor="secondary"
              aria-label="icon label tabs example"
            >
              <Tab icon={<DashboardIcon />} label="LIST" />
              <Tab icon={<MapIcon />} label="MAP" />
            </Tabs>
            <TabPanel value={value} index={0}>
              <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                style={{ marginTop: '0.5rem' }}
              >
                {cities?.length &&
                  cities?.map((city) => (
                    <CityItem city={city} key={city.name} />
                  ))}
              </Grid>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <Paper>
                <div
                  style={{ height: '80vh', width: '100%', marginTop: '1rem' }}
                >
                  <GoogleMapReact
                    bootstrapURLKeys={{
                      key: process.env.REACT_APP_GOOPLE_MAP_KEY,
                    }}
                    defaultCenter={{ lat: 53.893009, lng: 27.567444 }}
                    defaultZoom={7}
                  >
                    {cities?.length &&
                      cities?.map((city) => (
                        <Link
                          lat={city?.coordinates?.latitude}
                          lng={city?.coordinates?.longitude}
                          to={`/office/${city.id}`}
                          key={city.id}
                        >
                          <Tooltip
                            arrow
                            title={`${city.city}, ${city.address}`}
                          >
                            <LocationCityIcon
                              id={city.id}
                              fontSize="large"
                              color="secondary"
                            />
                          </Tooltip>
                        </Link>
                      ))}
                  </GoogleMapReact>
                </div>
              </Paper>
            </TabPanel>
          </>
        ) : (
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              height: '90vh',
            }}
          >
            <CircularProgress size={70} color="secondary" />
          </div>
        )}
      </Container>
    </>
  );
};
export default CitiesList;
