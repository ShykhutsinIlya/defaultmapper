import React from 'react';

import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import { Link } from 'react-router-dom';
import { STYLES_CONSTANTS } from '../../_constants/material.constants';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import Grid from '@material-ui/core/Grid';

const CityItem = ({ city }) => {
  const classes = STYLES_CONSTANTS.STYLES_CITY();
  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography
          className={classes.country}
          variant="h5"
          component="h2"
          color="secondary"
        >
          {city.country}
        </Typography>
        <Typography className={classes.city} color="primary">
          {city.city}
        </Typography>
        <Typography
          className={classes.name}
          variant="button"
          component="p"
          color="primary"
        >
          {city.name}
        </Typography>
        <Grid
          container
          className={classes.address}
          direction="row"
          alignItems="center"
          justifyContent="center"
        >
          <LocationOnOutlinedIcon color="secondary" fontSize={'medium'} />
          <Typography variant="body2" component="p" color="primary">
            {city.address}
          </Typography>
        </Grid>
      </CardContent>
      <CardActions className={classes.actionCityList}>
        <Button
          size="medium"
          className={classes.street}
          color="secondary"
          component={Link}
          to={`/office/${city.id}`}
          key={city.address}
        >
          Open
        </Button>
      </CardActions>
    </Card>
  );
};
export default CityItem;
