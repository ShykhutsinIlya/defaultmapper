import React, { useEffect } from 'react';
import './css/style.css';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { officeActions } from '../../redux/actions/office';
import { roomActions } from '../../redux/actions/room';
import { cityActions } from '../../redux/actions/city';
const Error = (props) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(officeActions.clearOfficeError());
    dispatch(roomActions.clearRoomError());
    dispatch(cityActions.clearCityError());
  }, []);
  
  return (
    <div id="notfound">
      <div className="notfound">
        <div className="notfound-404">
          <h3>Oopsie Woopsie</h3>
          <h1>
            <span>4</span>
            <span>0</span>
            <span>4</span>
          </h1>
        </div>
        <h2>we are sorry, but the page you requested was not found</h2>
        <Button
          component={Link}
          style={{ width: '150px' }}
          to="/"
          variant="contained"
          color="primary"
        >
          Go Home
        </Button>
      </div>
    </div>
  );
};

export default Error;
