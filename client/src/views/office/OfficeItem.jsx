import React from 'react';
import { STYLES_CONSTANTS } from '../../_constants/material.constants';
import { Container } from '@material-ui/core';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Card from '@material-ui/core/Card';
import { Grid } from '@material-ui/core';
import DomainDisabledIcon from '@material-ui/icons/DomainDisabled';

const OfficeItem = ({ office }) => {
  const classes = STYLES_CONSTANTS.STYLES_FLOOR();

  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
      <Container style={{marginBottom: "1rem"}}>
        <Breadcrumbs
          aria-label="breadcrumb"
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '1rem',
            marginBottom: '1rem',
          }}
        >
          <Typography variant="h5" color="secondary">
            {office.country}
          </Typography>
          <Typography variant="h5" color="primary">
            {office.city}
          </Typography>
          <Typography variant="h5" color="primary">
            {office.address}
          </Typography>
        </Breadcrumbs>
        <Paper elevation={0}>
          <> {office.floor_count ?
            office.floors.map((item) => (
              <Accordion
                expanded={expanded === 'floor' + item.number} 
                onChange={handleChange('floor' + item.number)}
                className={classes.accordion}
                disabled={!item.rooms.length}
                key={item.id}
              >
                <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography color="secondary">Floor {item.number} </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Grid container justifyContent={'center'}>
                    {item.rooms.map((room) => (
                      <Link
                        style={{ textDecoration: 'none' }}
                        to={`/room/${room.id}`}
                        key={room.id}
                      >
                        <Card
                          className={classes.card}
                          style={{
                            color:
                              '#' +
                              Math.floor(Math.random() * 16777215).toString(16),
                          }}
                        >
                          {room.number}
                        </Card>
                      </Link>
                    ))}
                  </Grid>
                </AccordionDetails>
              </Accordion>
            )) : 
            <Grid container>
              <Grid item xs={12} style={{textAlign: 'center', marginTop: '1rem'}}>
                <DomainDisabledIcon style={{ fontSize: 100 }} color="secondary" />
                <Typography style={{marginTop: '1rem', fontSize: 16, fontWeight: 'bold'}}variant="button" component="div" color="primary">There are no floors yet</Typography> 
              </Grid>
            </Grid>}
          </>
        </Paper>
      </Container>
  );
};
export default OfficeItem;
