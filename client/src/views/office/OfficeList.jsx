import React, { useEffect } from 'react';
import OfficeItem from './OfficeItem';
import { Redirect, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { officeActions } from '../../redux/actions/office';
import CircularProgress from '@material-ui/core/CircularProgress';
import './css/index.css';
import Header from '../../_component/Header';

const Office = () => {
  let { id } = useParams();
  const dispatch = useDispatch();
  const office = useSelector((state) => state.officeReducer.office);
  const loader = useSelector((state) => state.officeReducer.loader);
  const errorOffice = useSelector((state) => state.officeReducer.error);

  useEffect(() => {
    !office?.length && dispatch(officeActions.getOffice(id));
  }, []);

  return (
    <>
    {errorOffice && <Redirect to="/error" />}
      <Header />
      {!loader ? (
        office ? (
          <OfficeItem office={office} />
        ) : (
          <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '90vh',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%,-50%)',
          }}
        >
          <p>No offices</p>
        </div>
        )
      ) : (
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '90vh',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%,-50%)',
          }}
        >
          <CircularProgress size={70} color="secondary" />
        </div>
      )}
    </>
  );
};
export default Office;
