import React, { memo } from 'react';

import { CANVAS_CONSTANTS } from '../../_constants/canvas.constants';

const Canvas = ({
  room,
  sideBar,
  menuBar,
  setSelectedTable,
  selectedTable,
  setAction,
  tables,
  setTables,
  action,
  createTable,
  showError,
  user,
}) => {
  const allowedSize = 20;
  // triggering when mouse clicked down
  const handleMouseDown = (event) => {
    if (!user?.is_staff) {
      return;
    }
    const { clientX, clientY } = event;
    // cycle and if-statement for checking if current mouse position in tables position
    for (let i = 0; i < tables.length; i++) {
      const [x1, y1, x2, y2] = CANVAS_CONSTANTS.convertPoints(
        tables[i]?.x1,
        tables[i]?.y1,
        tables[i]?.x2,
        tables[i]?.y2,
        'toPixels'
      );
      if (
        event.clientX - sideBar.offsetWidth >= x1 &&
        event.clientX - sideBar.offsetWidth <= x2 &&
        event.clientY - menuBar.offsetHeight >= y1 &&
        event.clientY - menuBar.offsetHeight <= y2 &&
        action !== 'drawing'
      ) {
        // getting table at current mouse position when clicked
        const table = CANVAS_CONSTANTS.getTableAtPosition(
          clientX - sideBar.offsetWidth,
          clientY - menuBar.offsetHeight,
          tables
        );
        if (table) {
          const offsetX = clientX - sideBar.offsetWidth - x1;
          const offsetY = clientY - menuBar.offsetHeight - y1;
          setSelectedTable({ ...table, offsetX, offsetY });

          if (table.position === 'inside') {
            setAction('moving');
          } else {
            setAction('resizing');
          }
        }

        return;
      }
    }

    // if clicked on blank area, start drawing

    const id = tables[tables.length - 1]
      ? 'pop' + tables[tables.length - 1].id + 1
      : 'pop' + 0;
    const x1 = clientX - sideBar.offsetWidth;
    const y1 = clientY - menuBar.offsetHeight;

    const x2 = clientX - sideBar.offsetWidth;
    const y2 = clientY - menuBar.offsetHeight;
    const [convx1, convy1, convx2, convy2] = CANVAS_CONSTANTS.convertPoints(
      x1,
      y1,
      x2,
      y2,
      'toPercent'
    );
    const table = createTable(id, convx1, convy1, convx2, convy2);
    setTables((prevState) => [...prevState, table]);
    setSelectedTable(table);

    setAction('drawing');
  };
  const mainBlock = document.getElementById('container');
  // triggering when mouse moving
  const handleMouseMove = (event) => {
    if (!user?.is_staff) {
      return;
    }
    const { clientX, clientY } = event;
    const table = CANVAS_CONSTANTS.getTableAtPosition(
      clientX - sideBar.offsetWidth,
      clientY - menuBar.offsetHeight,
      tables
    );
    const [tableX1, tableY1, tableX2, tableY2] = CANVAS_CONSTANTS.convertPoints(
      selectedTable?.x1,
      selectedTable?.y1,
      selectedTable?.x2,
      selectedTable?.y2,
      'toPixels'
    );
    event.target.style.cursor = table
      ? CANVAS_CONSTANTS.cursorForPosition(table.position)
      : 'default';

    if (action === 'drawing') {
      const x2 = clientX - sideBar.offsetWidth;
      const y2 = clientY - menuBar.offsetHeight;

      updateTable(selectedTable.id, tableX1, tableY1, x2, y2);
    } else if (action === 'moving') {
      const { id, offsetX, offsetY, users } = selectedTable;

      const width = tableX2 - tableX1;
      const height = tableY2 - tableY1;

      const newx1 = clientX - sideBar.offsetWidth - offsetX;
      const newy1 = clientY - menuBar.offsetHeight - offsetY;

      const midX = (newx1 + newx1 + width) / 2;
      const midY = (newy1 + newy1 + height) / 2;
      if (
        midX >= mainBlock.offsetWidth ||
        midY >= mainBlock.offsetHeight ||
        midX <= 0 ||
        midY <= 0
      ) {
        return;
      }
      updateTable(id, newx1, newy1, newx1 + width, newy1 + height, users);
    } else if (action === 'resizing') {
      const { id, position, ...coordinates } = selectedTable;
      let { x1, y1, x2, y2 } = CANVAS_CONSTANTS.resizedCoordinates(
        clientX - sideBar.offsetWidth,
        clientY - menuBar.offsetHeight,
        position,
        coordinates
      );
      if (Math.abs(x1 - x2) < allowedSize) {
        x2 = x1 + allowedSize;
      }

      if (Math.abs(y1 - y2) < allowedSize) {
        y2 = y1 + allowedSize;
      }

      updateTable(id, x1, y1, x2, y2, selectedTable.users);
    }
  };

  // updating table params
  const updateTable = (id, x1, y1, x2, y2, users) => {
    const newx1 = x2 > x1 ? x1 : x2;
    const newx2 = x2 > x1 ? x2 : x1;
    const newy1 = y2 > y1 ? y1 : y2;
    const newy2 = y2 > y1 ? y2 : y1;
    const [convx1, convy1, convx2, convy2] = CANVAS_CONSTANTS.convertPoints(
      newx1,
      newy1,
      newx2,
      newy2,
      'toPercent'
    );
    const updatedTable = createTable(id, convx1, convy1, convx2, convy2, users);
    const tablesCopy = tables.map((elem) => {
      if (elem.id === id) {
        elem = updatedTable;
      }
      return elem;
    });
    setTables([...tablesCopy]);
  };

  // triggering when mouse releasing
  const handleMouseUp = (event) => {
    if (!user?.is_staff) {
      return;
    }
    // getting lastly created table
    let lastTable = tables[tables.length - 1];

    const [convx1, convy1, convx2, convy2] = CANVAS_CONSTANTS.convertPoints(
      lastTable?.x1,
      lastTable?.y1,
      lastTable?.x2,
      lastTable?.y2,
      'toPixels'
    );
    // checking if painted table greater than allowed size on x and y
    let isSmall =
      Math.abs(convx1 - convx2) <= allowedSize ||
      Math.abs(convy1 - convy2) <= allowedSize;
    if (convx2 === convx1 || convy2 === convy1) {
      const newTables = [...tables];
      newTables.pop();
      setTables([...newTables]);
    } else if (isSmall && action === 'drawing') {
      const newTables = [...tables];
      newTables.pop();
      setTables([...newTables]);
      showError();
    }

    setAction('none');
    setSelectedTable(null);
  };
  return (
    <canvas
      id="canvas"
      style={{
        backgroundImage: `url(${room?.image})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        backgroundSize: '100% 100%',
        zIndex: 20,
        position: 'absolute',
      }}
      onMouseDown={handleMouseDown}
      onMouseMove={handleMouseMove}
      onMouseUp={handleMouseUp}
      onTouchStart={handleMouseDown}
      onTouchMove={handleMouseMove}
      onTouchEnd={handleMouseUp}
    ></canvas>
  );
};

export default memo(Canvas);
