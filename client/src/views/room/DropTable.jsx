import React, { memo } from 'react';
import Paper from '@material-ui/core/Paper';
import { STYLES_CONSTANTS } from './style/material.constants';
import { Droppable } from 'react-beautiful-dnd';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import { CANVAS_CONSTANTS } from '../../_constants/canvas.constants';

const DropTable = ({ table, sideBar, menuBar, delTable, delUser }) => {
  const classes = STYLES_CONSTANTS.STYLES_ROOM();
  const [convx1,convy1,convx2,convy2] = CANVAS_CONSTANTS.convertPoints(table.x1, table.y1, table.x2, table.y2, 'toPixels')
  const x1 = convx2 > convx1 ? convx1 : convx2;
  const x2 = convx2 > convx1 ? convx2 : convx1;
  const y1 = convy2 > convy1 ? convy1 : convy2;
  const y2 = convy2 > convy1 ? convy2 : convy1;

  return (
    <Droppable droppableId={`${table.id}`}>
      {(provided) => (
        <div
          id={`${table.id}`}
          ref={provided.innerRef}
          {...provided.droppableProps}
          className={classes.tableItem}
          style={{
            width: x2 - x1,
            height: y2 - y1,
            top: y1 + menuBar.offsetHeight,
            left: x1 + sideBar.offsetWidth,
          }}
        >
          <div
            style={{
              flexDirection:
                table.x2 - table.x1 < table.y2 - table.y1 ? 'column' : 'row',
            }}
            className={classes.tableItemUser}
          >
            {table?.users?.map((user) => {
              return (
                <Paper
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    zIndex: '50',
                    justifyContent: 'center',
                    position: 'relative',
                    minWidth: '100px',
                    margin: 10,
                    padding: 5
                  }}
                  key={user.id}
                  id={`${user.id}`}
                  draggable={false}
                  elevation={3}
                >
                { user.image_url !== 'https://resources.bamboohr.com/images/photo_person_150x150.png' 
                ? <Avatar src={user.image_url} alt={user.first_name[0] + ' ' + user.last_name[0]} className={classes.avatarSmall} /> : 
                  <Avatar className={classes.emptySmallAvatar}> {user.first_name[0] + " " + user.last_name[0]} </Avatar>
                }
                <Typography variant="button" color="primary" style={{fontSize:"10pt", fontWeight: "700"}}>
                  {user.first_name} 
                </Typography>
                <Typography variant="button" color="primary" style={{fontSize:"10pt", fontWeight: "700"}}>
                  {user.last_name}
                </Typography>
                  <CloseIcon
                    onClick={() => delUser(table.id, user)}
                    variant="contained"
                    color="secondary"
                    className={classes.closeIcon}
                  />
                </Paper>
              );
            })}
            <CloseIcon
              onClick={() => delTable(table)}
              variant="contained"
              color="secondary"
              style={{
                position: 'absolute',
                zIndex: '50',
                cursor: 'pointer',
                top: '0',
                right: '0',
              }}
            />
          </div>
        </div>
      )}
    </Droppable>
  );
};

export default memo(DropTable);
