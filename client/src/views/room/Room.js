import React, {
  useEffect,
  useLayoutEffect,
  useMemo,
  memo,
  useState,
} from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { STYLES_CONSTANTS } from './style/material.constants';
import { Redirect, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { roomActions } from '../../redux/actions/room';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import rough from 'roughjs/bundled/rough.esm';
import Button from '@material-ui/core/Button';
import DropTable from './DropTable';
import Canvas from './Canvas';
import SearchBar from 'material-ui-search-bar';
import { userActions } from '../../redux/actions/user';
import { tableActions } from '../../redux/actions/table';
import UserList from './UserList';
import CircularProgress from '@material-ui/core/CircularProgress';
import Alert from '@material-ui/lab/Alert';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import { CANVAS_CONSTANTS } from '../../_constants/canvas.constants';
import Header from '../../_component/Header';

// creating rough table
const createTable = (id, x1, y1, x2, y2, users) => {
  const [newx1, newy1, newx2, newy2] = CANVAS_CONSTANTS.convertPoints(
    x1,
    y1,
    x2,
    y2,
    'toPixels'
  );
  const roughTable = generator.rectangle(
    newx1,
    newy1,
    newx2 - newx1,
    newy2 - newy1,
    { roughness: 0 }
  );
  return { id, x1, y1, x2, y2, roughTable, users };
};

// some variable for rough library ¯\_(ツ)_/¯
const generator = rough.generator();

function useWindowSize() {
  // Initialize state with undefined width/height so server and client renders match
  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });
  useEffect(() => {
    // Handler to call on window resize
    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    // Add event listener
    window.addEventListener('resize', handleResize);
    // Call handler right away so state gets updated with initial window size
    handleResize();
    // Remove event listener on cleanup
    return () => window.removeEventListener('resize', handleResize);
  }, []); // Empty array ensures that effect is only run on mount
  return windowSize;
}
const Room = () => {
  const sideBar = document.getElementById('side-bar');
  const menuBar = document.getElementById('appbar');
  // styles
  const classes = STYLES_CONSTANTS.STYLES_ROOM();
  // loading room from server
  const { id } = useParams();
  const dispatch = useDispatch();
  // getting current user
  const localData = JSON.parse(localStorage.getItem('user'));
  let userId = localData.user_id;
  const user = useSelector((state) => state.userInfoReducer.user);
  useEffect(() => {
    !user?.length && dispatch(userActions.getUserInfo(userId));
  }, []);

  const room = useSelector((state) => state.roomReducer.room);
  const errorRoom = useSelector((state) => state.roomReducer.error);
  const loaderUser = useSelector((state) => state.userReducer.loader);
  const userList = useSelector((state) => state.userReducer.users);

  const [delListTable, setDelListTable] = useState([]);
  const [showSave, setShowSave] = useState(false);
  const [showError, setShowError] = useState(false);
  // user state
  const [users, setUsers] = useState(userList);
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });
  // tables state
  const [tables, setTables] = useState([]);
  const [selectedTable, setSelectedTable] = useState(null);
  const [action, setAction] = useState('none');
  const [searchedUsers, setSearchedUsers] = useState(users);
  const [searchValue, setSearchValue] = useState('');
  // drawing tables at start
  const tableRender = (room) => {
    if (room?.tables) {
      const newTables = [];
      room?.tables?.map((table) => {
        if (table.points.length < 2) return;
        const x1 = table.points[0][0];
        const y1 = table.points[0][1];
        const x2 = table.points[1][0];
        const y2 = table.points[1][1];
        table.users = table.users.map(
          (user) =>
            (user = {
              ...user,
              start_work_time: '2021-07-23T09:48:04.385Z',
              end_work_time: '2021-07-23T09:48:04.385Z',
            })
        );
        const tableFromDB = createTable(table.id, x1, y1, x2, y2, table.users);
        newTables.push(tableFromDB);
      });
      setTables([...newTables]);
    }
  };

  useEffect(() => {
    dispatch(roomActions.getRoom(id));
    dispatch(userActions.getAllUsers());
  }, []);

  useEffect(() => {
    function handleResize() {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    window.addEventListener('resize', handleResize);
    handleResize();
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  useEffect(() => {
    const newUsers = users.filter((user) => {
      const fullName = (user?.first_name + ' ' + user?.last_name).toLowerCase();
      return fullName.includes(searchValue.toLowerCase());
    });
    setSearchedUsers(newUsers);
  }, [users, searchValue]);

  useEffect(() => {
    tableRender(room);
  }, [room]);

  useEffect(() => {
   setUsers(
      userList?.map(
        (user) =>
          (user = {
            ...user,
            start_work_time: '2021-07-23T09:48:04.385Z',
            end_work_time: '2021-07-23T09:48:04.385Z',
            user: user.id,
          })
      )
    );
  }, [userList]);

  // dragging users
  const handleOnDragEnd = (result) => {
    if (user?.is_staff) {
      return;
    }
    if (!result.destination) return;
    if (result.destination.droppableId === 'side-bar') {
      const items = Array.from(searchedUsers);
      const [reorderedItem] = items.splice(result.source.index, 1);
      items.splice(result.destination.index, 0, reorderedItem);
      setSearchedUsers(items);
    } else {
      const newTable = tables.map((elem) => {
        !elem.users && (elem.users = []);
        if (elem.users.length > 1) {
          return elem;
        }
        if (elem.id == result.destination.droppableId) {
          const items = Array.from(users);
          const indexUser = items.findIndex(
            (item) => item.id.toString() === result.draggableId
          );
          elem.users.push(items[indexUser]);
          const newUserList = items.filter(
            (user) => user.id !== items[indexUser].id
          );
          setUsers(newUserList);
        }
        return elem;
      });

      setTables([...newTable]);
    }
  };

  // setting and rendering canvas depending on tables
  useEffect(() => {
    const mainBlock = document.getElementById('container');

    const canvas = document.getElementById('canvas');
    const context = canvas.getContext('2d');

    context.clearRect(0, 0, canvas.width, canvas.height);

    canvas.width = mainBlock.offsetWidth;
    canvas.height = mainBlock.offsetHeight;

    const roughCanvas = rough.canvas(canvas);

    tables.forEach((table) => {
      const [newx1, newy1, newx2, newy2] = CANVAS_CONSTANTS.convertPoints(
        table.x1,
        table.y1,
        table.x2,
        table.y2,
        'toPixels'
      );
      const roughTable = generator.rectangle(
        newx1,
        newy1,
        newx2 - newx1,
        newy2 - newy1,
        { roughness: '0' }
      );
      roughCanvas.draw(roughTable);
    });
  });

  // saving tables on click
  const saveTables = (e) => {
    tables.map((newTable) => {
      const roomParams = {
        points: [
          [newTable.x1, newTable.y1],
          [newTable.x2, newTable.y2],
        ],
        status: 'No occupation',
        room: room.id,
        users: newTable.users ? [...newTable.users] : [],
      };
      if (room.tables.findIndex((table) => table.id === newTable.id) === -1) {
        dispatch(tableActions.postTable(JSON.stringify(roomParams)));
      } else {
        dispatch(
          tableActions.patchTable(JSON.stringify(roomParams), newTable.id)
        );
      }
    });

    delListTable.map((tableID) => dispatch(tableActions.deleteTable(tableID)));
    setShowSave(true);
    setTimeout(() => {
      setShowSave(false);
    }, 3000);
  };

  const delTable = (table) => {
    if (!user?.is_staff) {
      return;
    }
    table.users && setUsers([...users, ...table.users]);
    const newArray = tables.filter((elem) => elem.id !== table.id);
    setDelListTable([...delListTable, table.id]);
    setTables([...newArray]);
  };

  const delUser = (tableId, delUser) => {
    if (!user?.is_staff) {
      return;
    }
    const newTables = tables.map((table) => {
      if (table.id === tableId) {
        const elem = table.users.filter((user) => user.id !== delUser.id);
        table.users = [...elem];
        return table;
      }
      return table;
    });
    setTables([...newTables]);
    setUsers([...users, delUser]);
  };

  const searchUser = (name) => {
    setSearchValue(name);
  };

  const showErrorMessage = () => {
    setShowError(true);
    setTimeout(() => {
      setShowError(false);
    }, 3000);
  };

  return (
    <>
      {errorRoom && <Redirect to="/error" />}
      <Header />
      <DragDropContext onDragEnd={handleOnDragEnd}>
        <Grid container className={classes.root} id="row">
          <Droppable
            droppableId="side-bar"
          
          >
            {(provided) => (
              <Grid
                id="side-bar"
                className={classes.sidebar}
                {...provided.droppableProps}
                ref={provided.innerRef}
                style={{ display: user?.is_staff ? 'flex' : 'none' }}
              >
                <Paper className={classes.sidePaper}>
                  <SearchBar
                    onChange={(name) => searchUser(name)}
                    onRequestSearch={(name) => searchUser(name)}
                    onCancelSearch={() => searchUser('')}
                    style={{ position: 'absolute' }}
                  />
                  {!loaderUser ? (
                    <UserList searchedUsers={searchedUsers} />
                  ) : (
                    <CircularProgress className={classes.loader} />
                  )}
                  {provided.placeholder}
                </Paper>
              </Grid>
            )}
          </Droppable>

          <Grid id="container" className={classes.main}>
            <Paper className={classes.mainPaper}>
              {showSave && (
                <div className={classes.saveBlock}>
                  <Alert
                    style={{
                      width: '100%',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '25px',
                    }}
                    severity="success"
                  >
                    Successfully saved!
                  </Alert>
                </div>
              )}
              {showError && (
                <div className={classes.saveBlock}>
                  <Alert
                    style={{
                      width: '100%',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: '25px',
                    }}
                    severity="error"
                  >
                    Table is too small
                  </Alert>
                </div>
              )}

              {user?.is_staff ? (
                <Tooltip arrow title="Save">
                  <Fab
                    className={classes.saveButton}
                    style={{ zIndex: 100 }}
                    color="secondary"
                    aria-label="save"
                    onClick={saveTables}
                  >
                    <SaveOutlinedIcon />
                  </Fab>
                </Tooltip>
              ) : (
                <></>
              )}

              {/* <Button
              variant="contained"
              color="secondary"
              className={classes.saveButton}
              onClick={saveTables}
              style={{ zIndex: 100 }}
            >
              Save
            </Button> */}

              <Canvas
                room={room}
                sideBar={sideBar}
                menuBar={menuBar}
                setSelectedTable={setSelectedTable}
                selectedTable={selectedTable}
                createTable={createTable}
                action={action}
                setAction={setAction}
                tables={tables}
                setTables={setTables}
                showError={showErrorMessage}
                user={user}
              />
            </Paper>

            {tables.map((elem) => {
              return (
                <DropTable
                  table={elem}
                  sideBar={sideBar}
                  menuBar={menuBar}
                  delTable={delTable}
                  delUser={delUser}
                  key={elem.id}
                />
              );
            })}
          </Grid>
        </Grid>
      </DragDropContext>
    </>
  );
};

export default memo(Room);
