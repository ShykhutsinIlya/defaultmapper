import React, { memo, useEffect, useMemo, useState } from 'react';
import Paper from '@material-ui/core/Paper';
import { Draggable } from 'react-beautiful-dnd';
import { STYLES_CONSTANTS } from './style/material.constants';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';

const UserList = ({ searchedUsers }) => {
  const classes = STYLES_CONSTANTS.STYLES_ROOM();

  return (
    <div
      style={{
        marginTop: '50px',
        height: 'calc(100vh - 115px)',
        overflow: 'auto',
      }}
    >
      {searchedUsers.map((user, index) => {
        return (
          <Draggable key={user.id} draggableId={`${user.id}`} index={index}>
            {(provided) => (
              <Paper
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                ref={provided.innerRef}
                className={classes.userPaper}
                elevation={3}
              >
                { user.image_url !== 'https://resources.bamboohr.com/images/photo_person_150x150.png' 
                ? <Avatar src={user.image_url} alt={user.first_name[0] + "" + user.last_name[0]} className={classes.avatar} /> : 
                  <Avatar className={classes.emptyAvatar}> {user.first_name[0] + user.last_name[0]} </Avatar>
                }
                <Typography variant="button" color="primary" style={{fontWeight: "700"}}>
                  {user.first_name} 
                </Typography>
                <Typography variant="button" color="primary" style={{fontWeight: "700"}}>
                  {user.last_name}
                </Typography>
                <br />
              </Paper>
            )}
          </Draggable>
        );
      })}
    </div>
  );
};

export default memo(UserList);
