import { makeStyles, } from '@material-ui/core/styles';

const STYLES_ROOM = makeStyles(theme => ({
  root: {},
  sidebar: {
    flexGrow: 1,
    height: 'calc(100vh - 64px)',
    width: '10%',
    overflow: 'auto',
  },
  sidePaper: {
    height: '100%',
    width: '100%',
    overflow: 'hidden',
    position: 'relative',
  },
  draggObj: {},

  main: {
    flexGrow: 1,
    width: '90%',
  },
  mainPaper: {
    height: 'calc(100vh - 64px)',
    width: '100%',
    background: '#E8E7E3',
    position: 'relative',
  },
  mainImg: {
    width: '100%',
    height: '100%',
  },
  userImg: {
    height: '100%',
    width: '100%',
  },
  saveButton: {
    position: 'absolute',
    // width: '200px',
    // height: '50px',
    // borderRadius: '5px',
    bottom: '2rem',
    right: '2rem',
    padding: '35px'
    // margin: 'auto',
  },
  saveBlock: {
    position: 'absolute',
    width: '100%',
    top: '0px',
    left: '0',
    right: '0',
    margin: 'auto',
    zIndex: 100,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loader: {
    position: 'absolute',
    top: '10%',
    left: '35%',
    zIndex: 100,
  },
  closeIcon: {
    position: 'absolute',
    zIndex: '50',
    cursor: 'pointer',
    top: '0',
    right: '0',
  },
  tableItem: {
    position: 'absolute',
    overflow: 'auto',
  },
  tableItemUser: {
    position: 'relative',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  userPaper: {
    margin: '0.5rem',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  avatar: {
    margin: '1rem',
    width: '48px !important',
    height: '48px !important',
  },
  emptyAvatar: {
    margin: '1rem',
    backgroundColor: '#df2745 !important',
    width: '48px !important',
    height: '48px !important',
  },
  avatarSmall: {
    margin: '.5rem',
    width: '44px !important',
    height: '44px !important',
  },
  emptySmallAvatar: {
    margin: '.5rem',
    backgroundColor: '#df2745 !important',
    width: '44px !important',
    height: '44px !important',
  },
}));

export const STYLES_CONSTANTS = {
  STYLES_ROOM,
};
