from datetime import timedelta
import os

from django.core.wsgi import get_wsgi_application

from celery.task import periodic_task, task
from celery import Celery

application = get_wsgi_application()

from users.services.bamboo_api import BambooSaveClient

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')

app = Celery('config', broker='redis://redis:6379/0')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@periodic_task(run_every=timedelta(hours=6))
def debug_task():
    BambooSaveClient.create_users()
