from django.contrib import admin
from django.urls import path, include, re_path

from core.swagger.urls import schema_view
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from config import settings

swagger_urls = [
    re_path(r'api/swagger(?P<format>\.json|\.yaml)', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path(r'api/swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path(r'api/redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

api_urls = [
    path('users/', include('users.api.v1.urls')),
    path('', include('offices.api.v1.urls')),
    path('', include('rooms.api.v1.urls')),
    path('', include('tables.api.v1.urls'))
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(api_urls)),
]

urlpatterns += swagger_urls

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()