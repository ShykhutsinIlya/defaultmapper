from core.enums.base_enums import BaseEnum


class TableOccupationEnum(BaseEnum):
    FREE = 'No occupation'
    TEMPORAL = 'Temporal occupation'
    FULL = 'Full occupation'
