from django.contrib import admin

from floors.models import Floor


@admin.register(Floor)
class FloorAdmin(admin.ModelAdmin):
    list_display = ('id', 'number', 'office')
    list_display_links = ('id', 'number')
    search_fields = ('office',)
