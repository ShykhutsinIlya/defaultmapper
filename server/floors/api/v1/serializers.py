from rest_framework import serializers

from floors.models import Floor

from rooms.api.v1.serializers import ListRoomSerializer


class ListFloorSerializer(serializers.ModelSerializer):

    rooms = ListRoomSerializer(many=True)

    class Meta:
        model = Floor
        fields = ('id', 'number', 'rooms')
