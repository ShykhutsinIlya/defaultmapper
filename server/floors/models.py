from django.utils.translation import gettext_lazy as _
from django.db import models

from offices.models import Office


class Floor(models.Model):
    number = models.PositiveIntegerField(
        verbose_name=_('floor number'),
        default=1,
    )
    office = models.ForeignKey(
        to=Office,
        verbose_name=_('office'),
        related_name='floors',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'Floor #{self.number} of {self.office.name} at {self.office.address}'

    class Meta:
        unique_together = ('number', 'office')
