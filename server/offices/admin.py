from django.contrib import admin

from offices.models import Office, Coordinates


@admin.register(Coordinates)
class CoordinatesAdmin(admin.ModelAdmin):
    list_display = ('id', 'latitude', 'longitude')


@admin.register(Office)
class OfficeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'address', 'floor_count')
    list_display_links = ('id', 'name')
