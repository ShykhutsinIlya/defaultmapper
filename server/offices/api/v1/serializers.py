from rest_framework import serializers

from offices.models import Office, Coordinates

from floors.api.v1.serializers import ListFloorSerializer


class ListCoordinatesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Coordinates
        fields = ('latitude', 'longitude')


class ListOfficeSerializer(serializers.ModelSerializer):
    coordinates = ListCoordinatesSerializer()
    floor_count = serializers.ReadOnlyField()

    class Meta:
        model = Office
        fields = ('id', 'name', 'address', 'city', 'country', 'coordinates', 'floor_count')


class RetrieveOfficeSerializer(serializers.ModelSerializer):

    floor_count = serializers.ReadOnlyField()
    floors = ListFloorSerializer(many=True)

    class Meta:
        model = Office
        fields = ('id', 'name', 'address', 'city', 'country', 'floor_count', 'floors')
