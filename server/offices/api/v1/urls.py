from django.urls import path, include

from rest_framework.routers import SimpleRouter

from offices.api.v1.views import OfficeViewSet

office_router = SimpleRouter()
office_router.register('offices', OfficeViewSet)

urlpatterns = [
    path('', include(office_router.urls)),
]
