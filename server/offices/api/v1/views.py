from rest_framework import viewsets, mixins

from offices.api.v1.serializers import ListOfficeSerializer, RetrieveOfficeSerializer
from offices.models import Office


class OfficeViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin, mixins.ListModelMixin):
    serializer_classes = {
        'list': ListOfficeSerializer,
        'retrieve': RetrieveOfficeSerializer,
    }
    queryset = Office.objects.all()
    serializer_class_default = ListOfficeSerializer

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.serializer_class_default)
