from django.utils.translation import gettext_lazy as _
from django.db import models

from django_countries.fields import CountryField


class Coordinates(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()

    def __str__(self):
        return f'lat={self.latitude}, lon={self.longitude}'


class Office(models.Model):
    name = models.CharField(_('name of office'), max_length=128)
    address = models.CharField(verbose_name=_('address'), max_length=128, unique=True)
    city = models.CharField(verbose_name=_('city'), max_length=64)
    country = CountryField()
    coordinates = models.ForeignKey(
        to=Coordinates,
        verbose_name=_('coordinates'),
        related_name='office',
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return f'{self.name} at {self.address} {self.country}/{self.city}'

    @property
    def floor_count(self):
        return self.floors.count()
