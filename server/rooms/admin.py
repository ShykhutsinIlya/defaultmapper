from django.contrib import admin

from rooms.models import Room


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ('id', 'number', 'width', 'length', 'floor')
    list_display_links = ('id', 'number')
    search_fields = ('floor',)
