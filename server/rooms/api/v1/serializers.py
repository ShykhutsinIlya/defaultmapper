from rest_framework import serializers

from rooms.models import Room

from tables.api.v1.serializers import ListTableSerializer, RetrieveListTableSerializer


class ListRoomSerializer(serializers.ModelSerializer):

    class Meta:
        model = Room
        fields = ('id', 'number')


class RetrieveRoomSerializer(serializers.ModelSerializer):

    tables = RetrieveListTableSerializer(many=True)

    class Meta:
        model = Room
        fields = ('id', 'number', 'image', 'width', 'length', 'tables')
