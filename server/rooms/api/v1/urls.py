from django.urls import path, include

from rest_framework.routers import SimpleRouter

from rooms.api.v1.views import RoomViewSet

room_router = SimpleRouter()
room_router.register('rooms', RoomViewSet)

urlpatterns = [
    path('', include(room_router.urls)),
]
