from rest_framework import viewsets, mixins

from rooms.api.v1.serializers import RetrieveRoomSerializer
from rooms.models import Room


class RoomViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin, mixins.UpdateModelMixin):
    serializer_classes = {
        'retrieve': RetrieveRoomSerializer,
    }
    queryset = Room.objects.all().prefetch_related('tables__users')
    serializer_class_default = RetrieveRoomSerializer

    http_method_names = ('get', 'post', 'head', 'patch', 'delete')

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.serializer_class_default)
