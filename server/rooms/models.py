import os

from django.core.validators import MinValueValidator
from django.utils.translation import gettext_lazy as _
from django.db import models

from floors.models import Floor


class Room(models.Model):
    def image_path(self, filename):
        return f'images/rooms/{filename}'

    number = models.PositiveIntegerField(verbose_name=_('room number'))
    image = models.ImageField(upload_to=image_path)
    width = models.FloatField(verbose_name=_('width'), validators=(MinValueValidator(0),))
    length = models.FloatField(verbose_name=_('length'), validators=(MinValueValidator(0),))
    floor = models.ForeignKey(
        to=Floor,
        verbose_name=_('floor'),
        related_name='rooms',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'Room #{self.number}-{self.floor.number} of {self.floor.office.name}'

    class Meta:
        unique_together = ('number', 'floor')
