from django.contrib import admin

from tables.models import Table, TableUser


@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    list_display = ('id', 'room', 'status')
    list_display_links = ('id', 'room')
    list_filter = ('room',)
    search_fields = ('room',)


@admin.register(TableUser)
class TableUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'table', 'start_work_time', 'end_work_time')
    list_display_links = ('id', 'user',)
    search_fields = ('user', 'table')
