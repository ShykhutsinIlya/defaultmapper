from django.db import transaction

from rest_framework import serializers

from tables.models import Table

from tables.models import TableUser
from users.api.v1.serializers import ListUserSerializer
from users.models import User


class TableUsersSerializer(serializers.ModelSerializer):

    class Meta:
        model = TableUser
        extra_kwargs = {'table': {'required': False}}
        fields = ('user', 'table', 'start_work_time', 'end_work_time')


class CreateTableTableUsersSerializer(serializers.ModelSerializer):
    id = serializers.PrimaryKeyRelatedField(source='user', queryset=User.objects.all())

    class Meta:
        model = TableUser
        extra_kwargs = {'table': {'required': False}}
        fields = ('id', 'table', 'start_work_time', 'end_work_time')


class TableSerializer(serializers.ModelSerializer):
    points = serializers.ListField()
    users = CreateTableTableUsersSerializer(write_only=True, allow_null=True, many=True)

    def create(self, validated_data):
        users = validated_data.pop('users')
        table = super(TableSerializer, self).create(validated_data)
        users = [TableUser(**user, table=table) for user in users]
        TableUser.objects.bulk_create(users)
        table.save()
        return table

    def update(self, instance, validated_data):
        users = validated_data.pop('users')
        with transaction.atomic():
            table = super(TableSerializer, self).update(instance, validated_data)
            table.users.clear()
            if users:
                table_users = [TableUser(table=table, **table_user) for table_user in users]
                TableUser.objects.bulk_create(table_users)
            table.save()
        return table

    class Meta:
        model = Table
        fields = ('id', 'points', 'status', 'users', 'room')


class ListTableSerializer(serializers.ModelSerializer):
    points = serializers.ListField()
    users = CreateTableTableUsersSerializer(read_only=True, source='table_user', many=True)

    class Meta:
        model = Table
        fields = ('id', 'points', 'status', 'users', 'room')


class RetrieveListTableSerializer(serializers.ModelSerializer):
    points = serializers.ListField()
    users = ListUserSerializer(many=True)

    class Meta:
        model = Table
        fields = ('id', 'points', 'status', 'users', 'room')

