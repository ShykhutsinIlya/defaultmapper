from offices.api.v1.serializers import ListOfficeSerializer
from offices.models import Office

from tables.models import TableUser

from users.models import User


def get_table_user_location_data(user: User) -> dict:
    """Return location data of a given user"""

    table_user = TableUser.objects.select_related('table').filter(user=user).first()
    office_data = {
        'coordinates': None,
        'room_number': None,
        'address': None,
        'country': None,
        'city': None,
    }

    if table_user is not None:
        room_number = table_user.table.room.number
        office = Office.objects.filter(floors__rooms__tables=table_user.table).first()
        office_data = ListOfficeSerializer(office).data
        exclude_fields = ('id', 'name', 'floor_count')
        office_data = {k: v for k, v in office_data.items() if k not in exclude_fields}
        office_data.update(dict(room_number=room_number))

    return office_data
