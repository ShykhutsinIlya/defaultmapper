from django.db.models.signals import post_save
from django.dispatch import receiver

from core.enums.tables import TableOccupationEnum

from tables.models import Table


@receiver(post_save, sender=Table)
def update_table_status_on_table_create_and_update(sender, instance: Table, created: bool, **kwargs):
    """Update table status on table update and create"""
    table_occupation = {
        0: TableOccupationEnum.FREE,
        1: TableOccupationEnum.TEMPORAL,
        2: TableOccupationEnum.FULL,
    }
    status = table_occupation.get(instance.users.count(), TableOccupationEnum.FREE)
    sender.objects.filter(pk=instance.pk).update(status=status.value)
