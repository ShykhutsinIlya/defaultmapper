from django.urls import path, include

from rest_framework.routers import SimpleRouter

from tables.api.v1.views import TableView

from tables.api.v1.views import TableUserView

table_router = SimpleRouter()
table_router.register('tables', TableView)
table_router.register('table-users', TableUserView)

urlpatterns = [
    path('', include(table_router.urls)),
]

