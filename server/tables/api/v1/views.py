from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins

from tables.api.v1.serializers import ListTableSerializer, TableUsersSerializer, TableSerializer
from tables.models import TableUser, Table

from core.permissions import IsAdminOrReadOnly


class TableView(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    GenericViewSet
):
    serializer_classes = {
        'retrieve': TableSerializer,
        'create': TableSerializer,
        'update': TableSerializer,
        'list': ListTableSerializer
    }
    queryset = Table.objects.all().prefetch_related('users')
    serializer_class_default = TableSerializer

    http_method_names = ('get', 'post', 'head', 'patch', 'delete')

    def get_permissions(self):
        if self.action in ('create', 'update', 'delete'):
            return [permission() for permission in (IsAdminOrReadOnly,)]
        else:
            return super().get_permissions()

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.serializer_class_default)


class TableUserView(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    GenericViewSet
):
    serializer_classes = {
        'retrieve': TableUsersSerializer,
        'create': TableUsersSerializer,
        'partial_update': TableUsersSerializer,
    }
    queryset = TableUser.objects.all()
    serializer_class_default = TableUsersSerializer

    http_method_names = ('get', 'post', 'head', 'patch', 'delete')

    def get_permissions(self):
        if self.action in ('create', 'partial_update'):
            return [permission() for permission in (IsAdminOrReadOnly,)]
        else:
            return super().get_permissions()

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.serializer_class_default)
