from django.contrib.postgres.fields import ArrayField
from django.utils.translation import gettext_lazy as _
from django.db import models

from core.enums.tables import TableOccupationEnum

from rooms.models import Room

from users.models import User


class Table(models.Model):
    points = ArrayField(
        ArrayField(
            models.FloatField(),
            size=2
        )
    )
    room = models.ForeignKey(
        to=Room,
        verbose_name=_('room'),
        related_name='tables',
        on_delete=models.CASCADE
    )
    users = models.ManyToManyField(
        to=User,
        through='TableUser',
        related_name='table_users'
    )
    status = models.CharField(
        verbose_name=_('table occupation status'),
        max_length=32,
        choices=TableOccupationEnum.items(),
        default=TableOccupationEnum.FREE
    )

    def __str__(self):
        return f'Table in {self.room}'


class TableUser(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    table = models.ForeignKey(to=Table, on_delete=models.CASCADE, related_name='table_user')
    start_work_time = models.DateTimeField(verbose_name=_('start work time'))
    end_work_time = models.DateTimeField(verbose_name=_('end work time'))

    def __str__(self):
        return f'Table from {self.table.room.number} for {self.user}'
