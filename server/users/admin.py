from django.contrib.auth.admin import UserAdmin
from django.contrib import admin

from users.models import User


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'email',
                'password',
                'first_name',
                'last_name',
                'image_url',
                'is_staff',
                'is_superuser',
                'bamboo_id',
            )
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = (
        'email',
        'first_name',
        'last_name',
        'is_staff',
        'bamboo_id',
    )
    readonly_fields = ('bamboo_id',)
    search_fields = ('first_name', 'last_name', 'email')
    ordering = ('email', 'last_name', 'first_name')
    list_filter = ('is_staff', 'is_superuser')
    filter_horizontal = tuple()

    def get_readonly_fields(self, request, obj=None):
        """Return user readonly fields"""
        if obj:
            return self.readonly_fields + ('email',)
        return self.readonly_fields
