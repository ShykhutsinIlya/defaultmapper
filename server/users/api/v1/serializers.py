from rest_framework import serializers

from users.models import User


class ListUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
            'email',
            'is_staff',
            'bamboo_id',
            'image_url'
        )
