from django.urls import path, include

from rest_framework import routers

from users.api.v1.views import CustomConvertTokenView, UserViewSet

router = routers.DefaultRouter()
router.register('users', UserViewSet, basename='user')

urlpatterns = [
    path('auth/convert-token/', CustomConvertTokenView.as_view(), name='convert-token'),
    path('auth/', include('rest_framework_social_oauth2.urls'), name='auth_urls'),
]

urlpatterns += router.urls
