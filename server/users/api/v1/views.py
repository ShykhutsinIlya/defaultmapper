from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins

from rest_framework_social_oauth2.views import ConvertTokenView

from oauth2_provider.models import AccessToken

from tables.api.v1.services import get_table_user_location_data

from users.api.v1.serializers import ListUserSerializer
from users.models import User

from tables.models import TableUser


class CustomConvertTokenView(ConvertTokenView):
    """
    Implements an endpoint to convert a provider token to an access token
    The endpoint is used in the following flows:

    * Authorization code
    * Client credentials
    """

    def post(self, request, *args, **kwargs):
        response = super(CustomConvertTokenView, self).post(request, *args, **kwargs)
        user = AccessToken.objects.get(token=response.data.get('access_token')).user
        response.data.update(
            dict(
                user_id=user.pk,
                email=user.email,
                first_name=user.first_name,
                last_name=user.last_name,
                is_staff=user.is_staff,
            )
        )
        return response


class UserViewSet(
    GenericViewSet,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin
):
    queryset = User.objects.all()
    serializer_class = ListUserSerializer

    def get_queryset(self):
        if self.action == 'list':
            table_users = TableUser.objects.select_related('user').values_list('user', flat=True)
            users = User.objects.exclude(id__in=table_users)
        else:
            users = User.objects.all()

        return users

    def retrieve(self, request, *args, **kwargs):
        data = super().retrieve(request, *args, **kwargs).data
        user = get_object_or_404(User, pk=data.get('id'))
        location_data = get_table_user_location_data(user=user)

        return Response(data={**data, **location_data})
