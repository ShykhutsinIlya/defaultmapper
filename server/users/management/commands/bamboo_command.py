from django.core.management.base import BaseCommand
from users.services.bamboo_api import BambooSaveClient


class Command(BaseCommand):

    def handle(self, *args, **options):
        BambooSaveClient.create_users()
