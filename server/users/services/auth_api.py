import os

import jwt

from datetime import datetime

SECRET_KEY = os.getenv('SECRET_KEY')
JWT_ALGORITHM = os.getenv('JWT_ALGORITHM')


def token_generator(request, token_type: str):
    user = request.user
    created = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")

    token = jwt.encode(
        dict(
            email=user.email,
            type=token_type,
            created=created
        ),
        SECRET_KEY,
        algorithm=JWT_ALGORITHM
    )

    return token


def refresh_token_generator(request):
    return token_generator(request=request, token_type='refresh')


def access_token_generator(request):
    return token_generator(request=request, token_type='access')
