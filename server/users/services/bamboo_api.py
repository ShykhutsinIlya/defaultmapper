import json
import os

import requests

from PyBambooHR.PyBambooHR import PyBambooHR

from users.models import User


class BambooClient(PyBambooHR):

    def __init__(self, api_key, subdomain):
        self.custom_headers = {'Content-Type': 'application/json'}
        self.auth = (api_key, '')
        super().__init__(subdomain=subdomain, api_key=api_key)

    def get_employees(self, fields):
        response = requests.post(
            url=f'https://api.bamboohr.com/api/gateway.php/{self.subdomain}/v1/reports/custom/?format=json',
            headers=self.custom_headers,
            data=json.dumps({
                "title": "Office Mapper",
                "filters": {
                    "lastChanged": {
                        "includeNull": "no",
                        "value": "2020-01-06T8:00:00Z"
                     }
                 },
                "fields": fields
            }),
            auth=self.auth
        )

        return response.json()


class BambooSaveClient:

    @staticmethod
    def create_users():
        object_bamboo = BambooClient(os.environ.get('BAMBOO_TOKEN'), os.environ.get('SUBDOMAIN'))
        employees = object_bamboo.get_employees(fields=['firstName', 'lastName', 'workEmail', 'photoUploaded'])
        for employee in employees.get('employees'):
            User.objects.update_or_create(
                bamboo_id=employee.get('id'),
                defaults={
                    'first_name': employee.get('firstName'),
                    'last_name': employee.get('lastName'),
                    'email': employee.get('workEmail'),
                    'bamboo_id': int(employee.get('id')),
                    'image_url': employee.get('photoUrl')
                }
            )
